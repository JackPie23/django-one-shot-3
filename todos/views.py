from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todo_list_list(request):
    todos = TodoList.objects.all
    context = {
        "todos": todos
    }
    return render(request, "todo/list.html", context)


def todo_list_detail(request, id):
    todo_instance = TodoList.objects.get(id=id)
    context = {
        "todo_instance": todo_instance
    }
    return render(request, "todo/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todo/createlist.html", context)


def todo_list_update(request, id):
    edit_todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=edit_todo)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=edit_todo.id)
    else:
        form = TodoForm(instance=edit_todo)
    context = {
        "form": form,
        "edit_todo": edit_todo
    }
    return render(request, "todo/editlist.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todo/listdelete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todo/itemcreate.html", context)


def todo_item_update(request, id):
    edit_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=edit_item.list.id)
    else:
        form = TodoItemForm(instance=edit_item)
    context = {
        "form": form,
        "edit_item": edit_item

    }
    return render(request, "todo/edititem.html", context)
